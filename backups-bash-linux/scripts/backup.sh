#!/usr/bin/env bash
set -e

###########################################################################
# Backup Script
#
# Parameters:
#   1 - sub directory for backups
#       (will be appended to base backup directory)
#   2 - text file with list of directories / files to backup
#       (items need to all be absolute paths)
#
# External Dependencies
#   log.sh - https://gitlab.com/joeysbytes/bash-snippets/-/raw/main/log/log.sh
#
###########################################################################


###########################################################################
# Global Variables / Constants / Imports
###########################################################################

# Populated by arguments
BACKUP_DEST_DIR=""
BACKUP_ITEMS_FILE=""
BACKUP_DEST_SUB_DIR=""

# Logging control
TIMESTAMP=$(date +%Y%m%d_%H%M%S)
LOG_LEVEL="info"  # trace, debug, info, warn/warning, error, fatal
LOG_TO_FILE="/data/spool/32/backup_${TIMESTAMP}.log"  # empty string = no file
LOG_TO_CONSOLE="disable"  # enable, disable
LOG_TO_CONSOLE_COLOR="enable"  # enable, disable

# Other Constants
BASE_BACKUP_DIR="/data/backups" # where all backups are stored
ARCHIVE_BACKUP_DIR="/data/spool/15"
BACKUP_SCRIPTS_DIR="/data/svr1/scripts/backups"  # where this script lives
COMMON_SCRIPTS_DIR="${BACKUP_SCRIPTS_DIR}/../common"  # where to find log.sh

# These are handled by the script
SCRIPT_ARGUMENTS=""
SCRIPT_ARGUMENTS_LENGTH=0
BACKUP_ITEMS=()


###########################################################################
# Main Processing Logic
###########################################################################

function main() {
    setup_logger
    log_info "Backup script starting"

    # validations
    check_if_root
    check_arguments
    check_backup_dest_dir
    check_backup_items_file

    # backing up
    load_backup_items_file
    process_backup_items
    perform_sync

    # archiving
    archive_backup
    perform_sync

    log_info "Backup script ended"
}


###########################################################################
# Perform Backups
###########################################################################

# Load the backup items list file into memory
function load_backup_items_file() {
    log_trace "Loading backup items list"
    local item
    while read -r item
    do
        # skip blank lines and commented lines
        if [[ ! "${item}" =~ ^# ]] && [[ ! "${item}" =~ ^[\ ]{0,}$ ]]
        then
            if [ -f "${item}" ] && [ ! -L "${item}" ]
            then
                log_debug "Adding file to backup items list: ${item}"
                BACKUP_ITEMS+=("${item}")
            elif [ -d "${item}" ] && [ ! -L "${item}" ]
            then
                log_debug "Adding directory to backup items list: ${item}"
                BACKUP_ITEMS+=("${item}")
            else
                log_warn "Item not found or of unknown type, skipping: ${item}"
            fi
        fi
    done < "${BACKUP_ITEMS_FILE}"
}


# Loop through the backup items and back them up
function process_backup_items() {
    log_trace "Processing backup items list"
    local item
    for item in "${BACKUP_ITEMS[@]}"
    do
        if [ -f "${item}" ]
        then
            backup_file "${item}"
        elif [ -d "${item}" ]
        then
            backup_directory "${item}"
        fi
    done
}


# Given a full path file name, back it up
function backup_file() {
    local file_path
    file_path="${1}"
    log_info "Backing up file: ${file_path}"
    local file_path_dir
    file_path_dir=$(dirname "${file_path}")
    local full_backup_dest_dir
    full_backup_dest_dir="${BACKUP_DEST_DIR}${file_path_dir}"
    log_debug "Backup directory destination: ${full_backup_dest_dir}"
    build_dir "${file_path_dir}"
    log_trace "Syncing file: ${file_path}  to dir: ${full_backup_dest_dir}/"
    rsync --archive "${file_path}" "${full_backup_dest_dir}/"
}


# Given a full path directory, back it up
function backup_directory() {
    local dir_path
    dir_path="${1}"
    log_info "Backup up directory: ${dir_path}"
    local dir_path_parent
    dir_path_parent=$(dirname "${dir_path}")
    local full_backup_dest_dir
    full_backup_dest_dir="${BACKUP_DEST_DIR}${dir_path_parent}"
    log_debug "Backup directory destination: ${full_backup_dest_dir}"
    build_dir "${dir_path_parent}"
    log_trace "Syncing directory: ${dir_path}  to dir: ${full_backup_dest_dir}"
    rsync --archive --delete "${dir_path}" "${full_backup_dest_dir}"
}


# Given a directory path, build the tree with the same permissions
# at each level.
# Could not get rsync to do this itself, so we this is why this
# function was created.
function build_dir() {
    local dir_name
    dir_name="${1}"

    # use recursion to build the path from parents forward
    if [ ! "${dir_name}" == "/" ]
    then
        local dir_name_parent
        dir_name_parent=$(dirname "${dir_name}")
        if [ ! "${dir_name_parent}" == "/" ]
        then
            build_dir "${dir_name_parent}"
        fi
    fi

    # create the directory
    if [ ! "${dir_name}" == "/" ]
    then
        log_debug "Creating / updating directory permissions: ${BACKUP_DEST_DIR}${dir_name}"
        local orig_dir_permissions
        orig_dir_permissions=$(stat --format="%a" "${dir_name}")
        log_trace "Permissions of ${dir_name}: ${orig_dir_permissions}"

        local orig_dir_uid
        orig_dir_uid=$(stat --format="%u" "${dir_name}")
        log_trace "UID of ${dir_name}: ${orig_dir_uid}"

        local orig_dir_gid
        orig_dir_gid=$(stat --format="%g" "${dir_name}")
        log_trace "GID of ${dir_name}: ${orig_dir_gid}"

        if [ ! -d "${BACKUP_DEST_DIR}${dir_name}" ]
        then
            log_trace "Creating directory: ${BACKUP_DEST_DIR}${dir_name}"
            mkdir "${BACKUP_DEST_DIR}${dir_name}"
        fi

        local new_dir_permissions
        new_dir_permissions=$(stat --format="%a" "${BACKUP_DEST_DIR}${dir_name}")
        if [ ! "${orig_dir_permissions}" == "${new_dir_permissions}" ]
        then
            log_trace "Updating permissions for: ${BACKUP_DEST_DIR}${dir_name}"
            chmod "${orig_dir_permissions}" "${BACKUP_DEST_DIR}${dir_name}"
        fi

        local new_dir_uid
        new_dir_uid=$(stat --format="%u" "${BACKUP_DEST_DIR}${dir_name}")
        local new_dir_gid
        new_dir_gid=$(stat --format="%g" "${BACKUP_DEST_DIR}${dir_name}")
        # echo "${orig_dir_uid}:${orig_dir_gid} - ${new_dir_uid}:${new_dir_gid}"
        if [ ! "${orig_dir_uid}" == "${new_dir_uid}" ] || [ ! "${orig_dir_gid}" == "${new_dir_gid}" ]
        then
            log_trace "Updating uid/gid for: ${BACKUP_DEST_DIR}${dir_name}"
            chown "${orig_dir_uid}":"${orig_dir_gid}" "${BACKUP_DEST_DIR}${dir_name}"
        fi
    fi
}


# Perform a sync on the file systems, to clear out any write caches
function perform_sync() {
    log_info "Performing file system sync"
    sync
}


###########################################################################
# Archive Backup Directory
###########################################################################

# Take the backup folder, and compress it to the archive directory
function archive_backup() {
    local archive_file_name="${ARCHIVE_BACKUP_DIR}/${BACKUP_DEST_SUB_DIR}_${TIMESTAMP}.tar.gz"
    log_info "Archiving backup: ${BACKUP_DEST_DIR}, to: ${archive_file_name}"
    cd "${BASE_BACKUP_DIR}"
    tar -c "${BACKUP_DEST_SUB_DIR}" | gzip -9 > "${archive_file_name}"
}


###########################################################################
# Initialization / Initial Validations
###########################################################################

# Import and setup logger
function setup_logger() {
    source "${COMMON_SCRIPTS_DIR}/log.sh"
    eval "set_log_level_${LOG_LEVEL}"
    eval "${LOG_TO_CONSOLE}_log_to_console"
    eval "${LOG_TO_CONSOLE_COLOR}_log_color"
    set_log_file "${LOG_TO_FILE}"
    log_trace "Logger imported and level set"
    log_debug "Logging to console: ${LOG_TO_CONSOLE}"
    log_debug "Logging console color: ${LOG_TO_CONSOLE_COLOR}"
    if [ "${LOG_TO_FILE}" == "" ]
    then
        log_debug "Log file disabled"
    else
        log_debug "Log file set to: ${LOG_TO_FILE}"
    fi
}


# Check if this script is running as root
function check_if_root() {
    log_trace "Checking if running as root"
    local me
    me=$(whoami)
    if [ "${me}" == "root" ]
    then
        log_trace "root user verified"
    else
        log_error "Must be root to run this script"
        exit 1
    fi
}


# Print how to use this script
function print_usage() {
    log_error "USAGE (run as root): backup.sh destinationBackupDir backupList"
}


# Check the arguments passed in to the script
function check_arguments() {
    log_trace "Checking the number of arguments"
    log_debug "Number of arguments passed to script: ${SCRIPT_ARGUMENTS_LENGTH}"
    log_debug "Arguments: ${SCRIPT_ARGUMENTS[*]}"
    log_trace "Validating the number of arguments"
    if [ "${SCRIPT_ARGUMENTS_LENGTH}" -ne 2 ]
    then
        log_error "Invalid number of arguments: ${SCRIPT_ARGUMENTS_LENGTH}"
        log_error "ARGUMENTS: ${SCRIPT_ARGUMENTS[*]}"
        print_usage
        exit 1
    else
        log_trace "Number of arguments validated"
    fi

    BACKUP_DEST_SUB_DIR="${SCRIPT_ARGUMENTS[0]}"
    BACKUP_DEST_DIR="${BASE_BACKUP_DIR}/${BACKUP_DEST_SUB_DIR}"
    BACKUP_ITEMS_FILE="${SCRIPT_ARGUMENTS[1]}"
}


# Check the backup destination directory
function check_backup_dest_dir() {
    log_trace "Checking the backup destination directory"
    log_info "Backup destination directory: ${BACKUP_DEST_DIR}"
    log_trace "Checking that the backup destination directory exists"
    if [ ! -d "${BACKUP_DEST_DIR}" ]
    then
        log_error "Backup destination directory does not exist: ${BACKUP_DEST_DIR}"
        exit 1
    else
        log_trace "Backup destination directory exists: ${BACKUP_DEST_DIR}"
    fi
}


# Check the backup items list file
function check_backup_items_file() {
    log_trace "Checking the backup items list file"
    log_info "Backup items list file: ${BACKUP_ITEMS_FILE}"
    log_trace "Checking that the backup items list file exists"
    if [ ! -f "${BACKUP_ITEMS_FILE}" ]
    then
        log_error "Backup items list file does not exist: ${BACKUP_ITEMS_FILE}"
        exit 1
    else
        log_trace "Backup items list file exists: ${BACKUP_ITEMS_FILE}"
    fi
}


###########################################################################
# Begin Script
###########################################################################

SCRIPT_ARGUMENTS=("$@")
SCRIPT_ARGUMENTS_LENGTH=${#SCRIPT_ARGUMENTS[@]}
main
