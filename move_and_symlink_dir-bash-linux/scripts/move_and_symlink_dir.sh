#!/usr/bin/env bash
set -e

# This script will move a directory to another location,
# with the original permissions, and create a symlink to
# it from its original location.

ERRORS_LOGGED="false"
LOG_LEVEL=0
USER_NAME=""
USER_GROUP=""
SOURCE_DIR=""
SOURCE_DIR_EXISTS="false"
SOURCE_DIR_EMPTY="true"
DEST_DIR=""
DEST_DIR_EXISTS="false"
DEST_DIR_EMPTY="true"
CHANGED="false"
CONTINUE="true"


###########################################################################
# MAIN PROCESSING LOGIC
###########################################################################
function main() {
    if [ "${ERRORS_LOGGED}" == "false" ] && [ "${CONTINUE}" == "true" ]
    then
        check_if_root
    fi
    if [ "${ERRORS_LOGGED}" == "false" ] && [ "${CONTINUE}" == "true" ]
    then
        validate_parameters "$@"
    fi
    if [ "${ERRORS_LOGGED}" == "false" ] && [ "${CONTINUE}" == "true" ]
    then
        USER_NAME=$(logname)
        USER_GROUP=$(id -gn "${USER_NAME}")
        SOURCE_DIR=$(realpath -s "${1}")
        DEST_DIR=$(realpath -s "${2}")
        if [ $# -eq 3 ]
        then
            LOG_LEVEL=${3}
        fi
        logdebug "Log level set to: ${LOG_LEVEL}"
        loginfo "Source dir: ${SOURCE_DIR}"
        loginfo "Dest dir:   ${DEST_DIR}"
        loginfo "User:       ${USER_NAME}"
        loginfo "User group: ${USER_GROUP}"
    fi
    if [ "${ERRORS_LOGGED}" == "false" ] && [ "${CONTINUE}" == "true" ]
    then
        get_source_dir_info
    fi
    if [ "${ERRORS_LOGGED}" == "false" ] && [ "${CONTINUE}" == "true" ]
    then
        get_dest_dir_info
    fi
    if [ "${ERRORS_LOGGED}" == "false" ] && [ "${CONTINUE}" == "true" ]
    then
        move_and_symlink_dir
    fi
    if [ "${CHANGED}" == "false" ]
    then
        echo "No changes made"
    fi
    if [ "${ERRORS_LOGGED}" == "false" ]
    then
        exit 0
    else
        exit 1
    fi
}


###########################################################################
# MAIN FUNCTIONS
###########################################################################

#--------------------------------------------------------------------------
# Get information about the source directory
#--------------------------------------------------------------------------
function get_source_dir_info() {
    loginfo "Checking source dir: ${SOURCE_DIR}"
    # Check if source directory exists
    if [ -e "${SOURCE_DIR}" ]
    then
        logdebug "  Source dir exists"
        if [ -d "${SOURCE_DIR}" ]
        then
            logdebug "  Source dir is a directory"
            SOURCE_DIR_EXISTS="true"
            if [ -L "${SOURCE_DIR}" ]
            then
                logdebug "  Source dir is a symbolic link"
                local link_target
                link_target=$(readlink "${SOURCE_DIR}")
                if [ "${link_target}" == "${DEST_DIR}" ]
                then
                    loginfo "Source dir is already a symbolic link to dest dir"
                    CONTINUE="false"
                else
                    logerror "Source dir is a link to a different dest: ${link_target}"
                fi
            fi
        else
            logerror "Source is not a directory: ${SOURCE_DIR}"
        fi
    else
        logdebug "  Source dir does not exist"
    fi
    # Check if source directory is empty
    if [ "${SOURCE_DIR_EXISTS}" == "true" ] && \
       [ "${CONTINUE}" == "true" ] && [ "${ERRORS_LOGGED}" == "false" ]
    then
        local source_file_cnt
        source_file_cnt=$(expr `ls -a "${SOURCE_DIR}"|wc -l` - 2 || :)
        if [ "${source_file_cnt}" == "0" ]
        then
            logdebug "  Source dir is empty"
        else
            logdebug "  Source dir is not empty"
            SOURCE_DIR_EMPTY="false"
        fi
    fi
}


#--------------------------------------------------------------------------
# Get information about the destination directory
#--------------------------------------------------------------------------
function get_dest_dir_info() {
    loginfo "Checking destination dir: ${DEST_DIR}"
    # Check if dest dir exists
    if [ -e "${DEST_DIR}" ]
    then
        logdebug "  Destination dir exists"
        if [ -d "${DEST_DIR}" ]
        then
            logdebug "  Destination is a directory"
            DEST_DIR_EXISTS="true"
            if [ -L "${DEST_DIR}" ]
            then
                logerror "  Destination dir is a symbolic link"
            fi
        else
            logerror "  Destination is not a directory"
        fi
    else
        logdebug "  Destination dir does not exist"
    fi
    # Check if destination directory is empty
    if [ "${DEST_DIR_EXISTS}" == "true" ] && \
       [ "${CONTINUE}" == "true" ] && [ "${ERRORS_LOGGED}" == "false" ]
    then
        local dest_file_cnt
        dest_file_cnt=$(expr `ls -a "${DEST_DIR}"|wc -l` - 2 || :)
        if [ "${dest_file_cnt}" == "0" ]
        then
            logdebug "  Destination dir is empty"
        else
            logdebug "  Destination dir is not empty"
            DEST_DIR_EMPTY="false"
        fi
    fi
    # Check if both source dir and destination dir are both non-empty
    if [ "${SOURCE_DIR_EXISTS}" == "true" ] && [ "${SOURCE_DIR_EMPTY}" == "false" ] && \
       [ "${DEST_DIR_EXISTS}" == "true" ] && [ "${DEST_DIR_EMPTY}" == "false" ] && \
       [ "${CONTINUE}" == "true" ] && [ "${ERRORS_LOGGED}" == "false" ]
    then
        logerror "Source dir and destination dir are both not empty"
    fi
}


#--------------------------------------------------------------------------
# Perform the move and symlink of the directory
#--------------------------------------------------------------------------
function move_and_symlink_dir() {
    loginfo "Moving and symlinking directory"
    if [ "${DEST_DIR_EXISTS}" == "false" ]
    then
        local dest_user_name=""
        local dest_group_name=""
        local dest_mode=""
        if [ "${SOURCE_DIR_EXISTS}" == "false" ]
        then
            logdebug "  Create destination directory with default permissions"
            dest_user_name="${USER_NAME}"
            dest_group_name="${USER_GROUP}"
            dest_mode="755"
        else
            logdebug "  Create destination directory with source directory permissions"
            dest_user_name=$(stat -c "%U" "${SOURCE_DIR}")
            dest_group_name=$(stat -c "%G" "${SOURCE_DIR}")
            dest_mode=$(stat -c "%a" "${SOURCE_DIR}")
        fi
        mkdir "${DEST_DIR}"
        chown "${dest_user_name}":"${dest_group_name}" "${DEST_DIR}"
        chmod "${dest_mode}" "${DEST_DIR}"
        CHANGED="true"
    fi
    if [ "${SOURCE_DIR_EXISTS}" == "true" ]
    then
        if [ "${SOURCE_DIR_EMPTY}" == "false" ]
        then
            logdebug "  Move source files to destination directory"
            shopt -s dotglob
            mv "${SOURCE_DIR}"/* "${DEST_DIR}"/
            shopt -u dotglob
            CHANGED="true"
        fi
        logdebug "  Remove source directory"
        rmdir "${SOURCE_DIR}"
        CHANGED="true"
    fi
    logdebug "  Create symlink from source directory to destination directory"
    ln -s "${DEST_DIR}" "${SOURCE_DIR}"
    CHANGED="true"
}


###########################################################################
# VALIDATIONS
###########################################################################

#--------------------------------------------------------------------------
# Need root privileges to be able to copy permissions exactly
#--------------------------------------------------------------------------
function check_if_root() {
    local the_user
    the_user=$(whoami)
    if [ ! "${the_user}" == "root" ]
    then
        logerror "This script must be run with root privileges"
    fi
}

#--------------------------------------------------------------------------
# Validate the passed-in parameters
#--------------------------------------------------------------------------
function validate_parameters() {
    # Check number of parameters
    if [ $# -eq 0 ]
    then
        logerror "No parameters passed in"
        print_usage "$@"
    elif [ $# -lt 2 ] || [ $# -gt 3 ]
    then
        local parameters=""
        for p in "$@"
        do
            parameters="${parameters}${p} "
        done
        logerror "Invalid number of parameters: $#"
        logerror "Parameters found: ${parameters}"
        print_usage "$@"
    else
        # Check source dir != dest dir
        if [ "${1}" == "${2}" ]
        then
            logerror "Source dir = Destination dir: ${1}"
        fi
        # Check log level
        if [ $# -eq 3 ]
        then
            local valid_log_level="true"
            if ! [[ "${3}" =~ ^[0-9]+$ ]]
            then
                valid_log_level="false"
            elif [ ${3} -lt 0 ]
            then
                valid_log_level="false"
            elif [ ${3} -gt 2 ]
            then
                valid_log_level="false"
            fi
            if [ ! "${valid_log_level}" == "true" ]
            then
                logerror "Invalid log level: ${3}"
                print_usage "$@"
            fi
        fi
    fi
}


#--------------------------------------------------------------------------
# Output how to call this script
#--------------------------------------------------------------------------
function print_usage() {
    logerror "USAGE: ${0} srcDir destDir [logLevel 0-2]"
    logerror "  srcDir:   The source directory you want to move"
    logerror "  destDir:  The destination directory you to move under"
    logerror "  logLevel: 0 = final status only"
    logerror "            1 = info level"
    logerror "            2 = debug level"
}


###########################################################################
# UTILITIES
###########################################################################

#--------------------------------------------------------------------------
# Logging
#--------------------------------------------------------------------------
function logdebug() {
    if [ "${LOG_LEVEL}" -ge 2 ]
    then
        echo "DEBUG: ${1}"
    fi
}

function loginfo() {
    if [ "${LOG_LEVEL}" -ge 1 ]
    then
        echo "INFO:  ${1}"
    fi
}

function logerror() {
    ERRORS_LOGGED="true"
    echo "ERROR: ${1}" 1>&2
}


main "$@"
