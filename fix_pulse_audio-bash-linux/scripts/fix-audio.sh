# shellcheck shell=bash

# This script fixes an issue (at least with KDE on certain kernel versions)
# where the audio device keeps changing, or doesn't get detected property
# (shows dummy output).  I just run this at startup of KDE.
#
# My notes:
#   KDE Neon on 5.4 kernel - has this issue
#   KDE Neon on 4.x kernel - no issues
#   Manjaro on 5.7 kernel - has this issue
#   Manjaro on 5.8 kernel - no issues

# Delete pulse configuration
rm -rf "${HOME}/.config/pulse"

# Kill PulseAudio
#   Will auto-restart and detect/create new configuration
pulseaudio -k
