#!/usr/bin/env bash
set -e

# Add this script to cron @reboot
# It is safe to run this script if syncthing is already running.

BASE_DIR="/data/svr1/syncthing"

function main() {
    local pgrep_count
    pgrep_count=$(pgrep --count --full "syncthing serve" || :)
    if [ "${pgrep_count}" -eq 0 ]
    then
        start_syncthing
    fi
}

function start_syncthing() {
    local CONFIG_DIR="${BASE_DIR}/config"
    local DATA_DIR="${BASE_DIR}/data"

    local LOG_DIR="${BASE_DIR}/logs"
    local LOG_FILE="${LOG_DIR}/syncthing.log"
    local LOG_FLAGS=3
    local LOG_MAX_FILES=10
    local LOG_MAX_BYTES=10485760

    syncthing serve \
        --config="${CONFIG_DIR}" \
        --data="${DATA_DIR}" \
        --no-default-folder \
        --no-browser \
        --logfile="${LOG_FILE}" \
        --logflags="${LOG_FLAGS}" \
        --log-max-old-files="${LOG_MAX_FILES}" \
        --log-max-size="${LOG_MAX_BYTES}" >/dev/null 2>&1 &
}

main
