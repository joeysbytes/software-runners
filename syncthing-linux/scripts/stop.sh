#!/usr/bin/env bash
set -e

# It is safe to run this script if syncthing is not running.

function main() {
    local pgrep_count
    pgrep_count=$(pgrep --count --full "syncthing serve" || :)
    if [ "${pgrep_count}" -gt 0 ]
    then
        echo "Stopping Syncthing"
        local pgrep_id
        pgrep_id=$(pgrep --full "syncthing serve"|head -1)
        kill "${pgrep_id}"
    else
        echo "Syncthing not running"
    fi
}

main
