#!/usr/bin/env bash

source ./vars.sh
echo "Stopping Docker Container for: ${CONT_NAME}"
docker stop "${CONT_NAME}"
echo "Short pause..."
sleep 2
echo "Deleting Docker Container for: ${CONT_NAME}"
docker rm "${CONT_NAME}"
