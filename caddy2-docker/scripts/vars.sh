# shellcheck shell=bash
# shellcheck disable=SC2034

# Variables for Caddy v2 Docker Container

echo "Variables being read for: caddy2"

IMG_NAME="caddy"
IMG_TAG="2"

CONT_NAME="caddy2"

DIR_WWW_HOST="/data/svr1/caddy2/www"
DIR_WWW_CONT="/usr/share/caddy"
DIR_DATA_HOST="/data/svr1/caddy2/data"
DIR_DATA_CONT="/data"
DIR_CONFIG_HOST="/data/svr1/caddy2/config"
DIR_CONFIG_CONT="/config"

FILE_CADDYFILE_HOST="/data/svr1/caddy2/caddyfile/Caddyfile"
FILE_CADDYFILE_CONT="/etc/caddy/Caddyfile"

HTTP_PORT_HOST="80"
HTTP_PORT_CONT="80"
# HTTPS_PORT_HOST="443"
# HTTPS_PORT_CONT="443"
# ADMIN_PORT_HOST="2019"
# ADMIN_PORT_CONT="2019"
