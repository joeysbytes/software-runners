#!/usr/bin/env bash

source ./vars.sh
echo "Building Docker image for: ${CONT_NAME}"
docker pull "${IMG_NAME}":"${IMG_TAG}"
