#!/usr/bin/env bash

source ./vars.sh
echo "Starting Docker Container for: ${CONT_NAME}"
docker run \
  --name="${CONT_NAME}" \
  --publish "${HTTP_PORT_HOST}":"${HTTP_PORT_CONT}" \
  --volume="${DIR_WWW_HOST}":"${DIR_WWW_CONT}" \
  --volume="${DIR_CONFIG_HOST}":"${DIR_CONFIG_CONT}" \
  --volume="${DIR_DATA_HOST}":"${DIR_DATA_CONT}" \
  --volume="${FILE_CADDYFILE_HOST}":"${FILE_CADDYFILE_CONT}" \
  --detach=true \
  --restart unless-stopped \
  "${IMG_NAME}":"${IMG_TAG}"
